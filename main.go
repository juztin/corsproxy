package main

import (
	"flag"
	"fmt"
	"net/http"

	"github.com/juztin/roxy"
)

func main() {
	src := flag.Int("source", 8080, "The source port to listen on")
	dst := flag.Int("destination", 8000, "The destination port to proxy to")
	flag.Parse()

	p := roxy.New()
	rp := p.ForPattern("/", roxy.ToPort(*dst))
	rp.ModifyResponse = func(resp *http.Response) error {
		resp.Header.Add("Access-Control-Allow-Origin", "*")
		return nil
	}
	fmt.Println(http.ListenAndServe(fmt.Sprintf(":%d", *src), p))
}
